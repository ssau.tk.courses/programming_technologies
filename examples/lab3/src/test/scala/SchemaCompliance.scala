import java.nio.file.Path

import com.github.fge.jackson.JsonLoader
import com.github.fge.jsonschema.main.JsonSchemaFactory
import datatypes.ClientState
import org.scalatest.FunSuite

class SchemaCompliance extends FunSuite {
  test("checkClientStateSchemeCompliance") {
    val clientState = ClientState(lastMessageTimestamp = "2020-09-11T08:54:45.528807100Z")
    val clientStateFilePath = Path.of("./target/checkClientStateSchemeCompliance.json")
    val schemaPath = Path.of("./resources/schemas/v0.0.1/ClientStateSchema.json")

    util.Util.writeJsonToFile(clientState, clientStateFilePath)
    val jsonToValidate = JsonLoader.fromFile(clientStateFilePath.toFile)

    val sf = JsonSchemaFactory.byDefault()
    val schema = sf.getJsonSchema(JsonLoader.fromFile(schemaPath.toFile))

    val report = schema.validate(jsonToValidate)
    println(report)

    assert(report.isSuccess)
  }

}
