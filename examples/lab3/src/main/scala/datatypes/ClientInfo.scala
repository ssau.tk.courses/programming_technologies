package datatypes

import com.fasterxml.jackson.annotation.JsonProperty

case class ClientInfo (
  @JsonProperty("timestamp") timestamp:String = java.time.Instant.now().toString,
  @JsonProperty("login") login:String
)