package datatypes

import com.fasterxml.jackson.annotation.JsonProperty

case class ClientState (
  @JsonProperty("lastMessageTimestamp") lastMessageTimestamp:String
)
