package datatypes

import com.fasterxml.jackson.annotation.JsonProperty

case class Message(
    @JsonProperty("timestamp") timestamp:String = java.time.Instant.now().toString,
    @JsonProperty("senderId") senderId:String,
    @JsonProperty("receiverId") receiverId:String,
    @JsonProperty("body") body:String
)