package util

import java.nio.file.Path

trait SavableState {
  def loadState(statesDir:Path):Unit
  def saveState(statesDir:Path):Unit
}
