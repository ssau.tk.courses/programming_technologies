package client

import java.io.{BufferedReader, IOException, InputStreamReader}
import java.net.{InetSocketAddress, SocketAddress}
import java.nio.channels.{NonReadableChannelException, NotYetConnectedException, SelectionKey, Selector, SocketChannel}

import ch.qos.logback.classic.Level
import com.typesafe.scalalogging.{LazyLogging, Logger}
import datatypes.{ClientInfo, Message}
import org.slf4j.LoggerFactory
import ch.qos.logback.classic.Level
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.exc.MismatchedInputException
import com.sun.mail.util.SocketConnectException

import scala.collection.immutable.ArraySeq
import scala.collection.mutable
import scala.jdk.CollectionConverters._
import scala.reflect.ClassTag

class Client(host:String,
             port:Int,
             recieveCallback: Message=>Unit,
             login:String
            ) extends Runnable with LazyLogging {

  @volatile var isRunning = false
  private val selector = Selector.open()
  private var clientChannel: SocketChannel = _
  private val inbox = mutable.ArrayDeque[Message]()
  private var isInitialized = false

  def start(): Unit = {
    isRunning = true

    Runtime.getRuntime.addShutdownHook(new Thread() {
      override def run(): Unit = {
        Thread.currentThread().setName("CLEANER")
        if (isRunning) {
          logger.info("Closing connection.")
          isRunning = false
          Thread.sleep(10)
          if (clientChannel != null) clientChannel.close()
        }
      }
    })

    val clientThread = new Thread(this, "CLIENT")
    clientThread.start()
  }

  def leaveMessage(receiverId:String, body: String): Selector = {
    val msg = Message(senderId = this.login, receiverId = receiverId, body = body)
    inbox.addOne(msg)
    clientChannel.register(selector, SelectionKey.OP_WRITE)
    selector.wakeup()
  }

  def run (): Unit = {
    clientChannel = SocketChannel.open()
    try {
      clientChannel.connect(new InetSocketAddress(host, port))
      logger.info(s"Connected to $host:$port")
    } catch {
      case e: SocketConnectException =>
        logger.info("Connection failed.")
        System.exit(0)
    }

    clientChannel.configureBlocking(false)
    clientChannel.register(selector, SelectionKey.OP_WRITE)

    sendRecieveLoop()
  }

  private def sendRecieveLoop() = {
    while (isRunning) {
      selector.select()
      for (key <- selector.selectedKeys().asScala) {
        if (key.isValid && key.isReadable) {
          receiveFromServer()
        }
        if (key.isValid && key.isWritable) {
          sendToServer()
        }
      }
    }
  }

  private def sendToServer() = {
    if (!isInitialized) {
      val clientInfo = ClientInfo(login = login)
      send[ClientInfo](clientInfo)
      isInitialized = true
    }
    if (isInitialized && inbox.nonEmpty) {
      try {
        val msg = inbox.removeHead()
        send(msg)
      } catch {
        case e@(_: MismatchedInputException |
                _: IOException) =>
          logger.info(s"Client disconnected.")
          System.exit(0)
        case e@(_: NotYetConnectedException |
                _: NonReadableChannelException |
                _: JsonProcessingException) =>
          logger.error(e.getMessage)
      }
    }
  }

  private def send[T](msg: T)(implicit ct: ClassTag[T]) = {
    util.Util.send[T](msg, clientChannel)
    clientChannel.register(selector, SelectionKey.OP_READ)
  }

  private def receiveFromServer() = {
    try {
      val msg = util.Util.recieve[Message](clientChannel)
      if (msg.nonEmpty) {
        recieveCallback(msg.get)
      }
    } catch {
      case e@(_: MismatchedInputException |
              _: IOException) =>
        logger.info(s"Client disconnected.")
        System.exit(0)
      case e@(_: NotYetConnectedException |
              _: NonReadableChannelException |
              _: JsonProcessingException) =>
        logger.error(e.getMessage)
    }
  }
}

object Client extends LazyLogging {
  LoggerFactory.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME).asInstanceOf[ch.qos.logback.classic.Logger].setLevel(Level.INFO)

  val defaultArgs = Map(
    "host" -> "0.0.0.0",
    "port" -> "10000"
  )

  def uiLoop(client: Client): Unit = {
    Thread.currentThread().setName("UI")
    val consoleInput = new BufferedReader(new InputStreamReader(System.in))

    while(client.isRunning) {
      val line = consoleInput.readLine()
      if (line != null) {
        client.leaveMessage(util.Util.BROADCAST_RECIEVER_ID, line)
      }
    }
  }

  def main(args:Array[String]): Unit = {
    val argsMap = args.toSeq.sliding(2,2).map{ case ArraySeq(k,v) => (k.replace("--",""), v) }.toMap
      .withDefault(defaultArgs)
    val host = argsMap("host")
    val port = argsMap("port").toInt
    val login = argsMap.getOrElse("login", util.Util.genId(java.time.Instant.now()))

    val client = new Client(
      host,
      port,
      message => println(s"${message.senderId}: ${message.body}"),
      login = login)
    client.start()

    uiLoop(client)
  }
}