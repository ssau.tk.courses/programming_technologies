package server

import java.io.IOException
import java.net.{InetAddress, InetSocketAddress, ServerSocket, Socket, SocketAddress, SocketOption}
import java.nio.ByteBuffer
import java.nio.channels.{NonReadableChannelException, NotYetConnectedException, SelectionKey, Selector, ServerSocketChannel, SocketChannel}
import java.nio.file.{Files, Path}
import java.util.concurrent.{ConcurrentHashMap, Executor, Executors, TimeUnit}

import ch.qos.logback.classic.Level
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.exc.MismatchedInputException
import com.typesafe.scalalogging.LazyLogging
import datatypes.{ClientInfo, Message}
import org.slf4j.LoggerFactory

import scala.collection.immutable.ArraySeq
import scala.collection.mutable
import scala.jdk.CollectionConverters._

class Server(host:String, port:Int, val savedStatesDir:Path) extends Runnable with LazyLogging {
  private val connectedClients = new mutable.HashMap[Int, Client]()
  private val loggedinClients = new mutable.HashMap[String, Client]()
  private val selector = Selector.open()

  @volatile var isRunning = false

  def run(): Unit = {
    logger.info(s"Server is running on $host:$port.")
    val serverSocketChannel = ServerSocketChannel.open()
    serverSocketChannel.bind(new InetSocketAddress(host, port))
    serverSocketChannel.configureBlocking(false)
    serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT)

    sendRecieveAcceptLoop(serverSocketChannel)
  }

  private def sendRecieveAcceptLoop(serverSocketChannel: ServerSocketChannel) = {
    while (isRunning) {
      selector.select()
      for (key <- selector.selectedKeys().asScala) {
        if (key.isValid && key.isAcceptable)
          acceptNewClient(serverSocketChannel)
        if (key.isValid && key.isReadable)
          receiveClientData(key)
        if (key.isValid && key.isWritable)
          sendDataToClient(key)
      }
    }
  }

  private def acceptNewClient(serverSocketChannel: ServerSocketChannel) = {
    val clientChannel = serverSocketChannel.accept()
    if (clientChannel != null) {
      clientChannel.configureBlocking(false)
      clientChannel.register(selector, SelectionKey.OP_READ)

      val clientId = clientChannel.hashCode()
      val client = new Client(socketChannel = clientChannel)

      connectedClients.put(clientId, client)

      logger.debug(s"Connection accepted.")
    }
  }

  private def receiveClientData(key: SelectionKey) = {
    val clientChannel = key.channel().asInstanceOf[SocketChannel]
    val clientId = clientChannel.hashCode()
    val client = connectedClients(clientId)
    try {
      if (!client.isInitialized) {
        val clientInfo = util.Util.recieve[ClientInfo](clientChannel)
        if (clientInfo.nonEmpty) {
          loginClient(client, clientInfo)
        } else {
          throw new java.io.IOException("Bad client info. Initialization aborted.")
        }
      } else {
        val msg = util.Util.recieve[Message](clientChannel)
        if (msg.nonEmpty) {
          client.lastMessageTimestamp = msg.get.timestamp
          client.saveState(savedStatesDir)
          routeMessage(msg.get, clientId)
        }
      }
    } catch {
      case e @ (_: com.fasterxml.jackson.databind.exc.MismatchedInputException |
                _: java.io.IOException ) =>
        logger.error(e.getMessage, e.getCause)
        handleDisconnection(clientChannel, clientId)
      case e @ (_ : java.nio.channels.NotYetConnectedException |
                _ : java.nio.channels.NonReadableChannelException |
                _ : com.fasterxml.jackson.core.JsonProcessingException) =>
        logger.error(e.getMessage, e.getCause)
    }
  }

  private def loginClient(client: Client, clientInfo: Option[ClientInfo]) = {
    val login = clientInfo.get.login
    if (loggedinClients.keySet.contains(login)) {
      val msg = Message(body=s"$login is already connected.", receiverId = "", senderId = "SERVER")
      leaveMessageForClient(msg, client)
    } else {
      loggedinClients.put(clientInfo.get.login, client)
      client.login = clientInfo.get.login
      client.loadState(savedStatesDir)
      client.isInitialized = true
      val msg = if ("".equals(client.lastMessageTimestamp)) {
        Message(body = s"Hello new client with login ${client.login} !", receiverId = "", senderId = "SERVER")
      } else {
        Message(body = s"Welcome back ${client.login} !", receiverId = "", senderId = "SERVER")
      }
      leaveMessageForClient(msg, client)
      logger.info(s"Client ${client.login} logged in.")
    }
  }

  private def handleDisconnection(clientChannel: SocketChannel, clientId: Int) = {
    clientChannel.close()
    val disconnectedClient = connectedClients.remove(clientId)
    if (disconnectedClient.nonEmpty) {
      loggedinClients.remove(disconnectedClient.get.login)
      val msg = Message(body = s"Client ${disconnectedClient.get.login} left.", receiverId = "", senderId = "SERVER")
      leaveMessageForConnectedClients(msg)
    }
    logger.info(s"Client ${disconnectedClient.get.login} disconnected.")
  }

  private def sendDataToClient(key: SelectionKey) = {
    val clientChannel = key.channel().asInstanceOf[SocketChannel]
    val clientId = clientChannel.hashCode()
    val client = connectedClients(clientId)
    val clientInbox = client.inbox
    if (clientInbox.nonEmpty)
      try {
        val messageToSend = clientInbox.removeHead()
        client.lastMessageTimestamp = messageToSend.timestamp
        client.saveState(savedStatesDir)
        util.Util.send(messageToSend, clientChannel)
        clientChannel.register(selector, SelectionKey.OP_READ)
      } catch {
        case e@(_: MismatchedInputException |
                _: IOException) =>
          handleDisconnection(clientChannel, clientId)
        case e@(_: NotYetConnectedException |
                _: NonReadableChannelException |
                _: JsonProcessingException) =>
          logger.error(e.getMessage, e.getCause)
      }
  }

  private def routeMessage(msg: Message, senderId:Int) = {
    msg.receiverId match {
      case _ =>
        for ((recieverId, reciever) <- connectedClients.iterator) {
          if (recieverId != senderId) {
            leaveMessageForClient(msg, reciever)
          }
        }
    }
  }

  private def leaveMessageForConnectedClients(msg:Message) = {
    for ((recieverId, reciever) <- connectedClients.iterator)
      leaveMessageForClient(msg, reciever)
  }

  private def leaveMessageForClient(msg: Message, reciever: Client) = {
    reciever.inbox.addOne(msg)
    reciever.socketChannel.register(selector, SelectionKey.OP_WRITE)
  }

  def start(): Unit = {
    isRunning = true
    Runtime.getRuntime.addShutdownHook(new Thread() {
      override def run(): Unit = {
        if (isRunning) {
          logger.info("Closing connections.")
          isRunning = false
          Thread.sleep(10)
          for (c <- connectedClients.values) {
            try c.socketChannel.close()
          }
        }
      }
    })
    this.run()
  }
}

object Server extends LazyLogging {
  LoggerFactory.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME).asInstanceOf[ch.qos.logback.classic.Logger].setLevel(Level.DEBUG)

  val defaultArgs = Map(
    "host" -> "0.0.0.0",
    "port" -> "10000",
    "states_path" -> "./clientStates/"
  )

  // how socket connection and key.isConnectable are related?
  // how socket connection and socket.isConnected are related?
  def main(args:Array[String]): Unit = {
    val argsMap = args.toSeq.sliding(2,2).map{ case ArraySeq(k,v) => (k.replace("--",""), v) }.toMap
      .withDefault(defaultArgs)
    val host = argsMap("host")
    val port = argsMap("port").toInt
    val statesPath = Path.of(argsMap("statesPath")).toAbsolutePath

    if (Files.notExists(statesPath)) {
      logger.info(s"Path statesPath = ${statesPath} does not exist. Aborting.")
      System.exit(0)
    }

    logger.info(s"Using ${statesPath} for client states.")

    val server = new Server(host, port, statesPath)
    server.start()
  }
}