package server

import java.nio.channels.SocketChannel
import java.nio.file.Path

import com.typesafe.scalalogging.LazyLogging
import datatypes.{ClientState, Message}
import util.SavableState

import scala.collection.mutable

class Client(
  var login:String = "",
  var lastMessageTimestamp:String = "",
  val inbox:mutable.ArrayDeque[Message] = new mutable.ArrayDeque[Message](),
  val socketChannel:SocketChannel,
  var isInitialized:Boolean = false) extends SavableState with LazyLogging {

  private def getStateFilePath(statesDir:Path): Path = statesDir.resolve(s"${login}.json")

  def saveState(statesDir:Path):Unit = {
    val filePath = getStateFilePath(statesDir)
    val clientState = ClientState(lastMessageTimestamp = lastMessageTimestamp)
    util.Util.writeJsonToFile(clientState, filePath)
    logger.debug(s"Client $login state saved.")
  }

  def loadState(statesDir:Path):Unit = {
    val filePath = getStateFilePath(statesDir)
    val clientState = util.Util.readJsonFromFile[ClientState](filePath)
    if (clientState.nonEmpty) {
      lastMessageTimestamp = clientState.get.lastMessageTimestamp
      logger.debug(s"Client $login state loaded.")
    }
  }
}
