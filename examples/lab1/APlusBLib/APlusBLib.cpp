﻿// APlusBLib.cpp : Определяет экспортируемые функции для DLL.

#include "APlusBLib.h"


// Пример экспортированной функции.
APLUSBLIB_API int APlusB(int a, int b)
{
    return a + b;
}